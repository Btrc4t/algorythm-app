package com.buttercat.algorythmhub.model;

import android.content.SharedPreferences;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import com.buttercat.algorythmhub.model.definitions.Color;
import com.buttercat.algorythmhub.model.definitions.ESP32Node;
import com.buttercat.algorythmhub.model.definitions.Mode;
import com.buttercat.algorythmhub.model.definitions.Prefs;
import org.eclipse.californium.core.CoapResponse;
import timber.log.Timber;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A repository which provides access to nodes discovered on the network
 */
public class NodeRepository {
    /**
     * Singleton instance of this class
     */
    private static NodeRepository sInstance;
    private final CoapHelper mCoapHelper;
    private final MediatorLiveData<List<ESP32Node>> mLiveNodesList;
    private List<ESP32Node> mNodeList = new ArrayList<>();
    private final MutableLiveData<ESP32Node> mLiveNodeClicked;
    private SharedPreferences mPrefs;
    private NodeLookupHelper mNodeLookupHelper;
    /**
     * Default constructor which links the {@link NodeLookupHelper} and {@link CoapHelper}
     *  @param nodeLookupHelper a static reference of {@link NodeLookupHelper}
     * @param coapHelper a static reference of {@link CoapHelper}
     * @param sprefs
     */
    private NodeRepository(final NodeLookupHelper nodeLookupHelper, CoapHelper coapHelper, SharedPreferences sprefs) {
        mCoapHelper = coapHelper;
        mLiveNodesList = new MediatorLiveData<>();
        mLiveNodeClicked = new MutableLiveData<>();
        mNodeLookupHelper = nodeLookupHelper;
        mPrefs = sprefs;
        mLiveNodesList.addSource(mNodeLookupHelper.getNodesLiveData(), esp32Node -> {
            CoapResponse modeResponse = null;
            if (esp32Node == null) { // Service lost
                mNodeList = new ArrayList<>();
                mLiveNodesList.postValue(mNodeList);
            } else {
                modeResponse = mCoapHelper.queryEndpoint(esp32Node, Mode.ENDPOINT);
            }

            if (modeResponse == null) {
                Timber.e("NodeRepository: null mode endpoint response");
            } else {
                Timber.e("NodeRepository: mode endpoint response %s", modeResponse.getCode());
                Mode mode = Mode.fromInt(modeResponse.getPayload()[0]);
                String room = mCoapHelper.queryEndpoint(esp32Node, "room").getResponseText();
                esp32Node.setRoom(room);
                esp32Node.setMode(mode);
                byte[] rgb = mCoapHelper.queryEndpoint(esp32Node, Color.ENDPOINT).getPayload();
                esp32Node.setColor(new Color(rgb[0], rgb[1], rgb[2]));
                Prefs prefs = getNodePrefs(esp32Node);
                esp32Node.setPrefs(prefs);
                Timber.i("UPDATING NODE IN LIST: %s",esp32Node.getAddress());
                updateNodeInList(esp32Node);
            }

        });
    }

    /**
     * @param nodeLookupHelper a singleton {@link NodeLookupHelper} in case the instance must be created
     *
     * @param mPrefs
     * @return an instance of this class, {@link NodeRepository}
     */
    public static NodeRepository getInstance(final NodeLookupHelper nodeLookupHelper, final CoapHelper coapHelper, SharedPreferences mPrefs) {
        synchronized (NodeRepository.class) {
            if (sInstance == null) {
                sInstance = new NodeRepository(nodeLookupHelper, coapHelper, mPrefs);
            }
        }
        return sInstance;
    }

    /**
     * Observed LiveData will notify the observer when the data has changed.
     *
     * @return a {@link LiveData<List< ESP32Node >>} with the latest contents from the mDNS Lookup Helper
     */
    public LiveData<List<ESP32Node>> getNodesLiveData() {
        return mLiveNodesList;
    }

    public void notifyClickedItem(ESP32Node node) {
        mLiveNodeClicked.postValue(node);
    }

    public LiveData<ESP32Node> getNodeClickedLiveData() {
        return mLiveNodeClicked;
    }

    public ESP32Node getLastSelectedNode() {
        ESP32Node selectedNode;
        selectedNode = mLiveNodeClicked.getValue();
        if (selectedNode == null) {
            // Auto select node if no node is clicked but this method is called with nodes available
            if (!mNodeList.isEmpty()) selectedNode = mNodeList.get(0);
        }
        return selectedNode;
    }

    private Prefs getNodePrefs(ESP32Node node) {
        Prefs prefs = new Prefs();

        int[] preferences = {
                prefs.getAmpMax(),
                prefs.getAmpMin(),
                prefs.getFreqBlueStart(),
                prefs.getFreqBlueEnd(),
                prefs.getFreqGreenEnd(),
                prefs.getFreqRedEnd(),
                prefs.getAudioHoldIntensity()
        };
        CoapResponse coapResponse = mCoapHelper.queryEndpoint(node, Prefs.ENDPOINT);
        if (coapResponse == null) return prefs;
        byte[] prefBytes = coapResponse.getPayload();
        if ((prefBytes.length/2) == preferences.length) {
            for (int b = 0; b < prefBytes.length; b+=2) {
                preferences[b/2] = (((((int) prefBytes[b+1]) & 0xff) << 8)
                        | (((int) prefBytes[b]) & 0xff));
            }
        } else {
            Timber.e("getNodePrefs: got an unexpected size from the node: %s", (prefBytes.length/2));
        }
        prefs = new Prefs(preferences);
        return prefs;
    }

    public void updateNodeInList(ESP32Node node) {
        if (mNodeList.isEmpty()) {
            mNodeList.add(node);
            mLiveNodesList.postValue(mNodeList);
            mPrefs.edit().putString(node.getAddress(), node.getHostName()).apply();
            return;
        }
        int modified = 0;
        for (int n = 0; n < mNodeList.size(); n++) { // lists will always be short, is it worth optimizing?
            if (node.getHostName().contentEquals(mNodeList.get(n).getHostName())) {
                mNodeList.set(n, node);
                mLiveNodesList.postValue(mNodeList);
                mPrefs.edit().putString(node.getAddress(), node.getHostName()).apply();
                modified += 1;
            }
        }
        if (modified == 0) {
            mNodeList.add(node);
            mLiveNodesList.postValue(mNodeList);
            mPrefs.edit().putString(node.getAddress(), node.getHostName()).apply();
        }
    }

    public void addNodeFromIp(String ip) {
        mNodeLookupHelper.addNodeFromIp(ip);
    }

    public CoapResponse putCoapUpdate(ESP32Node node, String endpoint) {
        return mCoapHelper.putEndpoint(node, endpoint);
    }

    public boolean checkConnection(ESP32Node node) {
            return mCoapHelper.ping(node);
    }

    public void updateNodeList() {
        mNodeLookupHelper.discoverNetwork();
    }
}
